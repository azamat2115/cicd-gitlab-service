package com.example.cicdgitlabservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicdGitlabServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CicdGitlabServiceApplication.class, args);
    }

}
